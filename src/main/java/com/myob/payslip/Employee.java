package com.myob.payslip;

public class Employee {

    private String firstName;
    private String lastName;
    private long annualSalary;
    private double superRate;
    private String paymentStartDate;

    private int incomeTax;

    private int grossIncome;
    private int superAmount;



    private int netIncome;

    public Employee(){

    }

    public Employee(String firstName, String lastName, long annualSalary, double superRate, String paymentStartDate){
        this.firstName = firstName;
        this.lastName = lastName;
        this.annualSalary = annualSalary;
        this.superRate = superRate;
        this.paymentStartDate = paymentStartDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public long getAnnualSalary() {
        return annualSalary;
    }


    public String getPaymentStartDate() {
        return paymentStartDate;
    }



    public int getNetIncome(){
        return this.netIncome;
    }

    public void setNetIncome(int netIncome) {
        this.netIncome = netIncome;
    }


    public double getSuperRate() {
        return superRate;
    }



    public int getIncomeTax() {
        return incomeTax;
    }



    public int getSuperAmount() {
        return superAmount;
    }



    public void setIncomeTax(int incomeTax) {
        this.incomeTax = incomeTax;
    }


    public void setSuperAmount(int superAmount) {
        this.superAmount = superAmount;
    }

    public int getGrossIncome() {
        return grossIncome;
    }

    public void setGrossIncome(int grossIncome) {
        this.grossIncome = grossIncome;
    }

}
