package com.myob.payslip;

import java.util.List;
import java.util.stream.Collectors;

import static com.myob.payslip.CSVWriter.write;
import static com.myob.payslip.PaySlipGenerator.defaultDivisions;
import static com.myob.payslip.PaySlipGenerator.*;

public class PaySlipApplication {



    public void run(String inputFile, String outputFile){
        CSVReader reader = new CSVReader();

        PaySlipGenerator generator = new PaySlipGenerator(defaultDivisions.get());

        List<Employee> employeeList = reader
                .read(inputFile, entries -> entries.map(entry -> new EmployeeBuilder().build(entry))
                        .collect(Collectors.toList()));

        //java8 lambda is still ugly, but better than nothing.
         write.apply(outputFile).accept(employeeList.stream()
                .map(incomeTax.apply(generator.getDivision))
                 .map(grossIncome)
                 .map(netIncome)
                .map(getSuper)
                .map(format)
                .collect(Collectors.toList()));

    }

    public static void main(String[] args){
        if(args.length < 2){
            System.out.println("usage:java -jar payslip-1.0-SNAPSHOT.jar  <input file> <output file>");
        }
        PaySlipApplication paySlipApplication = new PaySlipApplication();
        paySlipApplication.run(args[0],args[1]);
    }


}

