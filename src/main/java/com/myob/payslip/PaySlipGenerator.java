package com.myob.payslip;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

public class PaySlipGenerator {

    private List<TaxDivision> taxDivisionList;


    public PaySlipGenerator(List<TaxDivision> divisions) throws RuntimeException {
        if(divisions == null || divisions.size() == 0) {
            throw new RuntimeException("divisions must not be null or empty");
        }
        this.taxDivisionList = divisions;
    }


    public final Function<Employee,TaxDivision> getDivision =  employee ->
          taxDivisionList.stream()
                .filter(taxDivision ->

                        employee.getAnnualSalary() > taxDivision.getMinLimit() && employee.getAnnualSalary() <= taxDivision.getMaxLimit()
                )
                .findAny()
                .orElseThrow(() -> new RuntimeException("Tax division not found!"));

    /**
     * using lambda expression here
     */
    public final static Function<Function<Employee, TaxDivision>, UnaryOperator<Employee>> incomeTax = (getDivision) -> (employee) -> {
        TaxDivision division = getDivision.apply(employee);
        if (employee.getAnnualSalary() < 0) {
            throw new RuntimeException("Invalid annual salary range!");
        }
        final int incomeTax = (int) Math.round((division.getAdditionCost() + (employee.getAnnualSalary() - division.getMinLimit()) * division.getTaxRate()) / 12);
        employee.setIncomeTax(incomeTax);
        return employee;
    };


    public final static UnaryOperator<Employee> grossIncome = employee -> {
        employee.setGrossIncome(Math.round(employee.getAnnualSalary() / 12));
        return employee;
    };


    public final static UnaryOperator<Employee> netIncome = employee -> {
        employee.setNetIncome(employee.getGrossIncome() - employee.getIncomeTax());
        return employee;
    };

    public final static UnaryOperator<Employee> getSuper = employee -> {
        employee.setSuperAmount((int) Math.round(employee.getGrossIncome() * employee.getSuperRate()));
        return employee;
    };

    public final static Function<Employee, String> format = employee -> {
        return employee.getFirstName() + " " + employee.getLastName() + "," + employee.getPaymentStartDate() + "," + employee.getGrossIncome() + "," + employee.getIncomeTax() + "," + employee.getNetIncome() + "," + employee.getSuperAmount();
    };


    public static final Supplier<List<TaxDivision>> defaultDivisions = () ->
            Arrays.asList(
                    new TaxDivision(18200, 37000, 0.19, 0),
                    new TaxDivision(37000, 80000, 0.325, 3572),
                    new TaxDivision(80000, 180000, 0.37, 17547),
                    new TaxDivision(180000, Integer.MAX_VALUE, 0.45, 54547)
            );


}
