package com.myob.payslip;

public class TaxDivision {
    private int maxLimit;

    private int minLimit;
    private double taxRate;
    private double additionCost;
    public TaxDivision(int minLimit, int maxLimit, double taxRate, double additionCost) {
        this.minLimit = minLimit;
        this.maxLimit = maxLimit;
        this.taxRate = taxRate;
        this.additionCost = additionCost;
    }

//    public void setMaxLimit(int maxLimit) {
//        this.maxLimit = maxLimit;
//    }
//
//    public void setMinLimit(int minLimit) {
//        this.minLimit = minLimit;
//    }
//
//    public void setTaxRate(double taxRate) {
//        this.taxRate = taxRate;
//    }
//
//    public void setAdditionCost(double additionCost) {
//        this.additionCost = additionCost;
//    }

    public int getMaxLimit() {
        return maxLimit;
    }

    public int getMinLimit() {
        return minLimit;
    }

    public double getTaxRate() {
        return taxRate;
    }

    public double getAdditionCost() {
        return additionCost;
    }
}