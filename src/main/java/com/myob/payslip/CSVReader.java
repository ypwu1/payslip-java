package com.myob.payslip;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Function;
import java.util.stream.Stream;


public class CSVReader {

	public static final <R>  R read(String fileName, Function<Stream<String>,R> fn) throws RuntimeException{
		try{
			return fn.apply(Files.lines(Paths.get(fileName)));
		}catch (IOException e) {
			throw new RuntimeException("Unable to read from file: " + fileName);
		}

	}
	
}
