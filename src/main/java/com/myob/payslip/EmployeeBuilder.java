package com.myob.payslip;

import java.util.Optional;

public class EmployeeBuilder {
    private String firstName;
    private String lastName;
    private long annualSalary;
    private double superRate;
    private String paymentStartDate;

    public EmployeeBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public EmployeeBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public EmployeeBuilder setAnnualSalary(long annualSalary) {
        this.annualSalary = annualSalary;
        return this;
    }

    public EmployeeBuilder setSuperRate(double superRate) {
        this.superRate = superRate;
        return this;
    }

    public EmployeeBuilder setPaymentStartDate(String paymentStartDate) {
        this.paymentStartDate = paymentStartDate;
        return this;
    }

    public Employee build() {
        return new Employee(firstName, lastName, annualSalary, superRate, paymentStartDate);
    }

    public Employee build(String entry) {
        String[] employeeDetails = entry.split(",");
        setFirstName(employeeDetails[0]);
        setLastName(employeeDetails[1]);
        setAnnualSalary(Long.parseLong(employeeDetails[2]));
        setSuperRate(fromPercentageStringToDouble(employeeDetails[3]).orElseThrow(() -> new RuntimeException("Could not parse Super rate!")));
        setPaymentStartDate(employeeDetails[4]);
        return new Employee(firstName, lastName, annualSalary, superRate, paymentStartDate);
    }


    public static Optional<Double> fromPercentageStringToDouble(String value) throws NumberFormatException {
        return !value.endsWith("%") ? Optional.empty() : Optional.of(Double.parseDouble(value.substring(0, value.length() - 1)) / 100);
    }
}
