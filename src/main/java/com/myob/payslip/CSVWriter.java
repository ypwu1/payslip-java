package com.myob.payslip;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

public class CSVWriter {

    public final static Function<String, Consumer<List<String>>> write = (String fileName) -> (List<String> entries) -> {
        try (BufferedWriter bw = Files.newBufferedWriter(Paths.get(fileName))) {
            for (String line : entries) {
                bw.write(line + "\n");
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to write to file");
        }
    };

}
