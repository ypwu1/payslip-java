package com.myob.payslip;

import org.junit.Before;
import org.junit.Test;

import java.util.function.Function;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class CSVReaderTest {

    Function<Stream<String>,String>  mockFunction;

    @Before()
    public void setUp(){
        mockFunction = s -> "foo";

    }

    @Test
    public void openInvalidFile(){
        Throwable e = catchThrowable(() -> {
            CSVReader.read("foo.txt",mockFunction);
        });

        assertThat(e).isNotNull();
        assertThat(e.getMessage()).isEqualTo("Unable to read from file: foo.txt");
    }

    @Test
    public void openValidFile(){
        Throwable e = catchThrowable(() -> {
            assertThat(CSVReader.read("input.txt",mockFunction)).isEqualTo("foo");

        });

        assertThat(e).isNull();
    }
}
