package com.myob.payslip;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.myob.payslip.CSVWriter.write;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class CSVWriterTest {
    private List<String>  mockEntries;

    @Before()
    public void setUp(){
        mockEntries = Arrays.asList("foo","bar");
    }

    @Test
    public void openValidFile(){
        Throwable e = catchThrowable(() ->
            write.apply("output.txt").accept(mockEntries)
        );

        assertThat(e).isNull();
    }

}
