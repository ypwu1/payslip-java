package com.myob.payslip;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.myob.payslip.PaySlipGenerator.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class PaySlipGeneratorTest {
    private Employee employee;

    @Before
    public void setUp() {
        employee = new Employee("Oscar", "Wu", 60050, 0.40, "01 March - 31 March");
    }

    @Test
    public void createPaySlipGeneratorWithNoDivision() {
        Throwable e = catchThrowable(() -> {
            PaySlipGenerator generator = new PaySlipGenerator(null);
        });
        assertThat(e).isNotNull();

        Throwable e2 = catchThrowable(() -> {
            PaySlipGenerator generator = new PaySlipGenerator(new ArrayList<>());
        });
        assertThat(e2).isNotNull();
    }

    @Test
    public void createPaySlipWithDivision() {
        Throwable e = catchThrowable(() -> {
            PaySlipGenerator generator = new PaySlipGenerator(defaultDivisions.get());
        });

        assertThat(e).isNull();
    }

    @Test
    public void getDivisionForEmployee() {
        List<TaxDivision> divisions = defaultDivisions.get();
        PaySlipGenerator generator = new PaySlipGenerator(divisions);
        assertThat(generator.getDivision.apply(employee)).isEqualTo(divisions.get(1));
    }


    @Test
    public void calculateTaxableIncome() {
        PaySlipGenerator generator = new PaySlipGenerator(defaultDivisions.get());
        assertThat(incomeTax.apply(generator.getDivision).apply(employee).getIncomeTax()).isEqualTo(922);
    }

    @Test
    public void calculateGrossIncome() {
        assertThat(grossIncome.apply(employee).getGrossIncome()).isEqualTo(5004);
    }

    @Test
    public void calculateNetIncome() {
        PaySlipGenerator generator = new PaySlipGenerator(defaultDivisions.get());
        employee.setNetIncome(1000);
        assertThat(incomeTax.apply(generator.getDivision)
                .andThen(grossIncome)
                .andThen(netIncome).apply(employee).getNetIncome()).isEqualTo(4082);
    }

    @Test
    public void calculateSupper() {
        assertThat(grossIncome
                .andThen(getSuper)
                .apply(employee).getSuperAmount()).isEqualTo(2002);
    }


}
