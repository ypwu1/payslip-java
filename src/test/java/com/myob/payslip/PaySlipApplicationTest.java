package com.myob.payslip;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class PaySlipApplicationTest {


    @Test
    public void createApplicationWithValidInputAndOutput(){
        Throwable e = catchThrowable(() -> {
           new PaySlipApplication().run("input.txt","output.txt");
        });

        assertThat(e).isNull();
    }
}
