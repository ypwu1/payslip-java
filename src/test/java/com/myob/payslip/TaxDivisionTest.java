package com.myob.payslip;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TaxDivisionTest {
    @Test
    public void createTaxDivision() {
        assertThat(new TaxDivision(10,10,0.45, 2929)).isNotNull();
    }
}
