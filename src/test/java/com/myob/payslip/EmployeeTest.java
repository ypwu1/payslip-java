package com.myob.payslip;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class EmployeeTest {

    private Employee employee;

    @Before
    public void setUp() {
        employee = new Employee("Oscar", "Wu", 65000, 0.40, "01 March - 31 March");
    }

    @Test
    public void testIfEmployeeInstanceIsCreated() {
        assertThat(employee).isNotNull();
    }


}
