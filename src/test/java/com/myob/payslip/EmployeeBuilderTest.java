package com.myob.payslip;

import junit.framework.TestCase;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class EmployeeBuilderTest{

    @Test
    public void createNewEmployeeBuilder(){
        assertThat(new EmployeeBuilder()).isNotNull();
    }

    @Test
    public void createNewEmployee(){
        Employee employee = new EmployeeBuilder()
                .setAnnualSalary(60000)
                .setFirstName("Oscar")
                .setLastName("Wu")
                .setSuperRate(0.16)
                .setPaymentStartDate("01 March - 02 March")
                .build();
        assertThat(employee).isNotNull();

        assertThat(employee.getAnnualSalary()).isEqualTo(60000);
        assertThat(employee.getFirstName()).isEqualTo("Oscar");
        assertThat(employee.getLastName()).isEqualTo("Wu");
        assertThat(employee.getPaymentStartDate()).isEqualTo("01 March - 02 March");
        assertThat(employee.getSuperRate()).isEqualTo(0.16);
    }
}
